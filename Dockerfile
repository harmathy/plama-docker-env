FROM kdeneon/plasma:dev-unstable

ARG UID

USER root

RUN usermod -u $UID neon &&\
    chown -R neon:neon /run/neon &&\
    mkdir /build &&\
    chown neon:neon /build

USER neon
WORKDIR /home/neon
CMD ["bash", "-l"]


