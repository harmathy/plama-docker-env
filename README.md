Develop plasma with docker
==========================

Set up your development environment to use docker to develop KDE plasma software
without cluttering up your host system.

This will create a docker image based on the KDE neon docker images. The default
user `neon` will be set to your host UID to share the source code location with
the host system.


Build the image
---------------

Install docker and add your user to the group `docker`. Then build the image:

    docker build --build-arg UID="$(id -u)" -f Dockerfile -t plasma-dev .


Setup the container and start developing
----------------------------------------

Let's say we want to start developing *plasma-desktop* and assuming that you put
your source code into `$HOME/src` we create a container:

    docker create -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v $HOME/src/:/home/neon/src --name plasma-desktop -t -i plasma-dev

* `-e DISPLAY=$DISPLAY` and `-v /tmp/.X11-unix:/tmp/.X11-unix` allows the
  container to access the host X-server.
* `-v $HOME/src/:/home/neon/src` maps the source code location into the
  container.
* `-t` and `-i` let the container run interactively.

Start the continer:

    docker container start plasma-desktop

Now we install build dependencies and setup the build:

    docker container exec plasma-desktop sudo apt-get update
    docker container exec plasma-desktop sudo apt-get -y build-dep plasma-desktop
    docker container exec -w /build/ plasma-desktop cmake -DCMAKE_INSTALL_PREFIX=/usr /home/neon/src/kde/plasma-desktop

Now you can start developing and run for building and installing:

    docker container exec -w /build/ plasma-desktop make -j4
    docker container exec -w /build/ plasma-desktop sudo make install

For testing run your command in the container:

    docker container exec plasma-desktop kcmshell5 fonts


Links
-----

This is inspired by a [blog entry][1] by Jos van den Oever.
Thanks to Jonathan Riddell for providing the [kde neon docker images][2]

[1]: https://www.vandenoever.info/blog/2017/07/23/developing-kde-with-docker.html
[2]: https://github.com/KDE/docker-neon
