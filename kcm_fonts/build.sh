#!/bin/bash

docker container start plasma-desktop
docker container exec -w /build/kcms/fonts plasma-desktop make -j4
docker container exec -w /build/kcms/fonts plasma-desktop sudo make install

